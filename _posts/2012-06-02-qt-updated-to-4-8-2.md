---
title: 'Qt updated to 4.8.2'
date: 2012-06-02 00:00:00 
layout: post
---

<p>Just a quick update to let you know that tonight we updated our Qt version to 4.8.2. Upstream changelog can be found here: <a href="https://qt.gitorious.org/qt/qt/blobs/4.8/dist/changes-4.8.2">https://qt.gitorious.org/qt/qt/blobs/4.8/dist/changes-4.8.2</a>.</p>