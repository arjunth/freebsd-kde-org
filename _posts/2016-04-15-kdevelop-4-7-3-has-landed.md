---
title: 'KDevelop 4.7.3 has landed'
date: 2016-04-15 00:00:00 
layout: post
---

<p>
<a href="https://www.kdevelop.org/">KDevelop</a> has been updated in the official ports tree to the latest version, 4.7.3, released for KDE4.
    </p>