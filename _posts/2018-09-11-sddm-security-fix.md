---
title: "SDDM security fix"
date: 2018-09-11 00:00:00
layout: post
---

The SDDM port has been updated to version 0.17_1, to
backport fixes from the 0.18 branch related to session-reuse.
Users are encouraged to upgrade, although the default configuration
in FreeBSD is not affected.
