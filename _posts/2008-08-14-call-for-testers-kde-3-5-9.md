---
title: 'Call for testers: KDE 3.5.9'
date: 2008-08-14 00:00:00 
layout: post
---

<p>Max has spent some time dragging KDE 3 into shape, and we would like to make it available for test. Max writes: <i>"KDE 3.5.9 was released six months ago and 3.5.10 is coming soon.  I'm not sure we have time to get 3.5.10 for FreeBSD 7.1/6.4 release, but we can do KDE 3.5.9.<br />
			KDE 3.5.9 is ready in area51 repo, it builds fine on tinderbox (7.0-RELEASE, i386). It will be nice to test it on other platforms and releases. If there is no problem we can update KDE 3 ports anytime."</i></p><p>area51 was switched to IPv6-only recently. If you have IPv6 connection, you can access area51 via anonymous CVS:</p><pre>
$ cvs -d:ext:anoncvs@orm.arved.priv.at:/home/kde-FreeBSD co area51
</pre><p>If you have no IPv6, you can get an area51 checkout snapshot <a href="http://people.FreeBSD.org/~miwi/kde3/kde359.tgz">here</a>.</p>