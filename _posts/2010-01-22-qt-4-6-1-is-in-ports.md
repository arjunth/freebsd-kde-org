---
title: 'Qt 4.6.1 is in ports'
date: 2010-01-22 00:00:00 
layout: post
---

<p>Qt 4.6.1 has been committed to ports. See original list of changes for <a href="http://qt.nokia.com/developer/changes/changes-4.6.1">Qt 4.6.1</a> and <a href="http://qt.nokia.com/developer/changes/changes-4.6.0">Qt 4.6.0</a>.</p>