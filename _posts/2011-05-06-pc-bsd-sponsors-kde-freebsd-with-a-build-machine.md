---
title: 'PC-BSD sponsors KDE/FreeBSD with a build machine'
date: 2011-05-06 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is receiving assistance from <a href="http://pcbsd.org">PC-BSD</a> again. During last months things have gone a bit slowly: the main problem was we missed a package build system. Luckily, miwi@ managed to find a solution with Kris Moore&#8217;s help, and now we&#8217;re back on full speed.</p><p>We&#8217;d like to say thanks to Kris Moore, Josh Paetzel, Dru Lavigne, the <a href="http://pcbsd.org">PC-BSD</a> project and of course <a href="http://ixsystems.com">iXsystems</a> for their generous donation of a test machine for KDE.</p>