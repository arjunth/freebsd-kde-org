---
title: 'KDE Software Compilation 4.4.5 in ports'
date: 2010-06-29 00:00:00 
layout: post
---

<p>KDE SC ports have been updated to 4.4.5. See <a href="http://www.kde.org/announcements/announce-4.4.5.php">official announcement</a> for details.</p>