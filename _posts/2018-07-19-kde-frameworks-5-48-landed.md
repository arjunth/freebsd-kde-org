---
title: "KDE Frameworks 5.48 Landed"
date: 2018-07-19 00:00:00
layout: post
---

      The latest monthly version of KDE Frameworks is available
      in the official FreeBSD ports tree.
