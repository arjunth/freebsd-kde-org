---
title: 'KDE SC 4.7 roadmap'
date: 2011-08-26 00:00:00 
layout: post
---

<p>What&#8217;s keeping <b>KDE 4.7 still out</b> of FreeBSD? Well, the high number of tarballs (and thus ports) in which it was split for this release. Actually they&#8217;re not too much, but they require a deep scan to update dependencies. We&#8217;ll probably be ready for a <i>Call for tests</i> in <b>few days</b>, and we would even like to provide <b>test packages</b> to make the process easier and get more feedback!<br />
			<b>KDE PIM 4.7</b> will also be available, but it probably won&#8217;t be ready to be committed along with other ports.</p>