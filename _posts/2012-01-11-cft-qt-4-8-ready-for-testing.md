---
title: 'CFT: Qt 4.8 ready for testing'
date: 2012-01-11 00:00:00 
layout: post
---

<p>We'd like to inform you all that <a href="http://FreeBSD.kde.org/area51.php">area51</a> trunk is now featuring <b>Qt 4.8</b>. While it should be working (yet untested, feedback welcome), we suggest you not to test it along with KDE SC 4.7, as it won't work fine.</p>