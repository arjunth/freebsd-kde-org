---
title: 'KDE 4.2.1 in ports'
date: 2009-03-09 00:00:00 
layout: post
---

<p>KDE 4.2.1 has been committed to ports.</p><p>UPDATING:</p><pre>
20090309:
  AFFECTS: users of multimedia/phonon
  AUTHOR: kde@FreeBSD.org

  multimedia/phonon port has been split into phonon itself, phonon-xine
  and phonon-gstreamer backends. After updating phonon port you have
  to install at least one backend. phonon-xine backend is recommended
  for KDE.
</pre>