---
title: 'KDE Plasma 5.8 LTS in area51'
date: 2016-10-05 00:00:00 
layout: post
---

<p>
The KDE community has published an LTS (long term support) release
of <a href="https://dot.kde.org/2016/10/04/kde-20-plasma-58-lts-out-now-comprehensive-features">KDE Plasma 5.8</a>.
This version is available from the unofficial ports tree for KDE-FreeBSD,
<a href="https://freebsd.kde.org/area51.php">area51</a>.
    </p>