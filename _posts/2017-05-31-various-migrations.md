---
title: "Various Migrations"
date: 2017-05-31 00:00:00
layout: post
---

    Two parts of the KDE/FreeBSD initiative have migrated to other
    homes this week. The website has largely moved to the  [KDE Community Wiki](https://community.kde.org/FreeBSD)
    This will make it easier to keep the website up-to-date and
    for incidental contributions by users.
    The source repository for the unoffical KDE ports -- which is where they
    are developed before being added to the main FreeBSD ports tree --
    is now in github, alongside similar ports trees from xorg@ and
    gnome@. This simplifies the workflow of contributing updates to
    the KDE ports in the tree and unifies that workflow with the
    official ports tree.
