---
title: 'KDE SC 4.6.2 in ports'
date: 2011-04-07 00:00:00 
layout: post
---

<p>The KDE/FreeBSD team is pleased to announce April updates for KDE Software Compilation: 4.6.2, codename "Congrats". Read the full announcement here:<br />
			<a href="http://kde.org/announcements/announce-4.6.2.php">http://kde.org/announcements/announce-4.6.2.php</a>.</p><p>Special thanks to Raphael Kubo da Costa who ported the release.</p>