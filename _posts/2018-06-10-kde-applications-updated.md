---
title: "KDE Applications Updated"
date: 2018-06-10 00:00:00
layout: post
---

      The latest update to KDE Applications -- a wide range
      of productivity, home, office and entertainment applications --
      has been committed to the official FreeBSD ports tree.
      Look for KDE Applications 18.04.2 updates to be available
      from packages as well in a few days time.
