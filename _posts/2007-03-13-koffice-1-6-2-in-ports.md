---
title: 'KOffice 1.6.2 in ports'
date: 2007-03-13 00:00:00 
layout: post
---

<p>KOffice 1.6.2 has been committed to ports.</p><p>For more information, read the <a href="http://www.koffice.org/announcements/announce-1.6.2.php">official announcement</a> and the <a href="http://www.koffice.org/announcements/changelog-1.6.2.php">changelog</a>.</p>