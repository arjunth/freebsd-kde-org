--- 
title: "Alan Eldridge 1961 - 2003"
layout: page
---
Alan Eldridge (AlanE) passed away on 6 June 2003 in Denver, Colorado, USA, apparently the result of a self-induced overdose.

Born on 15 December 1961, Alan moved from Iowa to New York City some 15 years ago to make a life for himself as a first rate Unix programmer. Recently, however, his professional and personal life was severely disrupted, and as a result, Alan moved to Denver to start anew.

A member of the KDE on FreeBSD Core Team, both a FreeBSD and KDE committer and maintainer of numerous FreeBSD ports, Alan will be sorely missed by all those of us who knew him personally, by those who knew him only through his work and, indeed, by the entire FreeBSD and KDE communities at large.

Among Alan's many open source accomplishments were:

* KDE on FreeBSD Core Team member;
* member of the team creating ports and binary packages for KDE on FreeBSD;
* maintainer of KDE kdesu module;
* maintainer of 47 FreeBSD ports, in particular: Python, CUPS, FAM, Grip, autoconf, and automake, and former maintainer of Phoenix and Zope;
* three major independent projects: newfile, ruby-cdialog, and xmlrpc-jukebox;
* member of the SourceForge team maintaining BSD FAM.

<table style="margin: auto;">
	<tr>
		<td><a href="img/alane1.jpg"><img src="img/alane1-thumb.png" style="height: 105px; width: 140px;" /></a></td>
		<td><a href="img/alane2.jpg"><img src="img/alane2-thumb.png" style="height: 105px; width: 140px;" /></a></td>
	</tr>
</table>

## Reflections and rememberences of Alan Eldridge

Alan's contributions to our team should not be underestimated or forgotten. When he was on his game, I know few people as productive, and he pushed us all to do better, be cleverer, work harder.

I refuse to whitewash the man. Alan was quick to point out when he thought you were trying to sugar coat something. Alan was the archetypical geek at times, difficult to work with in a team, sometimes hard to get along with. He was sometimes bristling and difficult, sometimes naive about how his abrasiveness appeared to others, and yet could be totally artless and charming, should it be pointed out to him.

Alan was smart and funny. He had a quick and often very sharp wit, and wasn't afraid to use it. An eclectic at heart, he turned me onto music I'd have otherwise never listened to, and books I'd otherwise never have read, films I wouldn't have watched, and you could have an intelligent disagreement with him on the wildest topics, so long as they weren't personal.

Watching someone selfdestruct and not being able to help them is a painful thing to go through, whether it's from up close or from afar. The last few months, when things started to not go so well, he became a lot harder to deal with, incommunicative, withdrawing from his previous sociable self, no longer happy to idle on IRC or chatter in email.

Alan had a lot to contribute, and people who really did care what happened to him. It was easy to be angry at him, but the reason he managed to inspire such anger is that I always knew that he was capable of so much more and better. I don't know if he understood that, or that if I didn't like him so much, I wouldn't have tried so hard.

It's stating the obvious to say that Alan's life was in chaos, and he was in a lot of pain. Choosing to take the way out of this pain that he did, it is very easy to feel bitter, and guilty, and even to feel guilty for not feeling guilty, and I think it's important that we acknowledge this, and allow ourselves to feel it.

**....**

My relationship with Alan reminded me of how Internet relationships were hardly much different from real life ones. They're both full of fun, happy times, and, of course, pain. When I met Alan I thought I'd found someone who had not only the skills but the attitude and desire to help the KDE-FreeBSD project in some ways that I'd been doing exclusively for a number of years. As 2002 wore on, it seemed likely that he would be helping out in this manner. I was very happy about that, and looked forward to working with him for the foreseeable future.

Then things took a wrong turn around August or September. Someone at a company in NYC wrongly fired him. We were all up in arms about it, and he told us his lawyer thought they had a case, but that the company would drag it on in courts and it'd be a long time before he saw anything from it. So he dropped the case. Everyone was sad to hear about that. We all figured that since he was skilled, he'd get a job before the end of 2002, for sure. Unfortunately, that never happened.

In February 2003, Alan's time in New York City was up. He decided to move to Denver to live with a fellow member of KDE-FreeBSD. We all thought it was a great thing for Alan, since NYC was such a terrible place to live rentwise and jobwise. Everyone figured he would at least find SOME job, any job, since living costs were much lower in Denver. That never happened either. Then, after about 3-4 months of no luck, due to lack of money, he was going to be evicted. Alan was in the process of looking for a new place to live when one night, he decided to try dying again. Unfortunately, this time he succeeded.

Every time I went back and looked at the history, I got confused. Why did Alan choose this way out? I've decided that I'll never know, and will leave it at that.

But what I do know is that there were many hours of time that I spent talking to him about how to run some of FruitSalad's resources, how to best handle an upgrade of the KDE ports in FreeBSD, arguing over geeky things like "Why use object-oriented programming?", python vs. ruby, and stuff like that. These were the great times I had with Alan. I will remember them forever. I hope that Alan at least died peacefully and not in pain.

Rest in peace, Alan. Thank you for all your help and the great times you brought.

**....**

While Alan and I didn't always see eye to eye, and in fact frequently argued like cat and dog, I always had a respect for his technical skills. Quite often, he would figure out a brilliant workaround for a problem, one that had been evading me for hours or even days.

Alan had this way of making us laugh, usually at completely random moments, with a silly little comment that you just weren't expecting. He would point us at an mp3, quite often something with twisted lyrics, and we would all end up listening to it.

We will miss his hard work on the KDE-FreeBSD project. He really put a lot into it, and I like to think it gave him something back during a difficult period of his life.

I believe that Alan helped with packaging KDE with one of the Linux distributions before he started helping with FreeBSD packaging, so while many people might not know his name, they've quite possibly benefited from the hard work he did for KDE for several years.

Like him or hate him, I _will_ miss him.

**....**

I remember somewhat distinctly when Alan first started contributing to the kde-freebsd mailing list, and when he first started popping in our IRC channel. Funny and very clued were our first impression, and he lived up to it. With his take-charge attitude, he was very effective in everything he did.

My friends and I went to LinuxWorld that year, and I made it a point of meeting Alan (exchanged cell numbers so we'd actually find each other). We had some good technical and other discussions between him and my friends. Everyone had the same impression: "what a smart guy... and funny too!".

There is no doubt that Alan will be sorely missed by all. Alan was an asset to not only KDE-FreeBSD, but our lives in general.

**....**

I remember Alan as an acerbic individual - a severe character, ready to point out if you're wrong, quick to anger if you ignored his advice. But, like a lemon, it's also good for you, taken properly. Alan was quick with a quixotic comment, willing to take some advice, and always ready with an anecdote.

Looking back on my correspondence with Alan, it seems we wrote back and forth on some technical matters, but I remember him mostly from IRC. That was where his wit and technical skills could really shine, during our rapid banter with its wildly erratic topics. We were all shocked and horrified when he was held up at gunpoint in NYC, and spent a long time talking. But here Alan's life was already on the skids, it seems, and later he became more withdrawn, less talkative. More than a few words were rare, and I never got the chance to ask "hey, Alan, what's up?" before it was too late.

The only photo's I saw of Alan were those posted after Alan's death. He looked totally different from what I pictured, and somehow I had been convinced that he was 55. And now I'll never know what he's really like - all that's left is some 7-bit ASCII rambling and a feeling of "oh Alan, we hardly knew ye."

**....**

With Alan, one would never know how things would turn out. If he was in a good mood he was one of the most clever people I have ever met, if he was in a bad mood he was one of the worst people I have ever met.

This said and done, he has said and taught me things that I will remember for the rest of my life.

Alan and I shared a wicked music taste and one song I will always keep close is by Warren Zevon, called "My Shit's Fucked Up". I played it after I got the news of Alan's death and the memories came back to me. It got me thinking, how is it possible that a clever guy like Alan could not find a proper job and settle down with his cats and live a quiet life ever after? The answer to this I will never get.

Many long nights we have spent talking about insane things, some related to the projects we were involved in, others just plain garbage amongst two friends.

Some people say one cannot get to know people on the Internet. Well, they just have no clue. I considered Alan a friend no different than any real life friends I have.

If there is such a thing as reincarnation, I pray that Alan has moved onto a better life, less troubled than his previous one.

**....**

My sincere condolences.. I did not know Alan, but I know how tight-knit the FreeBSD community is and how much this affects everyone, whether they were friends with him or not.

I hope this sad event is also a strong reminder to people that "FreeBSD" has always been far more than a mere collection of code, the actual code being perhaps one of the least significant reasons that people are drawn to it and stay around for so long. FreeBSD is first and foremost a community, for some a community even closer than family. As with all families, there are frequently squabbles with harsh words exchanged and feelings hurt, but I hope that no one lets such incidents come between them and the fundamental realization that, after this much time spent in one another's company, we're still family whether we all get along or not. When someone in our family dies, we all grieve.

I may not be as active in FreeBSD these days as I wish I could be, and I think a certain ebb and flow of involvement with the actual coding work is only natural given life's other demands and changing circumstances, but I'm still extremely proud of this community and the strength it has had to accomplish things that no single one of us could ever have accomplished by ourselves. Far more than that, it's also given all of us a world-wide community of friends and people we can rely on to share a cup of coffee, or even their homes, no matter where in the world we might be. This is a kindness I experienced many times in my own travels throughout the world and can never express enough thanks to the FreeBSD community for.

We can never get Alan back and no words can properly express that loss, but perhaps his death can serve as the strongest possible reminder to all of us just what it is we all really came to FreeBSD for and what's most important about us being here.

**....**

I never got to know Alan on a personal level very well. He was ambitious and smart and funny, and a pleasure to work with. Despite not knowing him well, I considered him a friend, and was always glad to get a private email from him, even if it was, as late, not the most upbeat of news.

I'm sorry I never got to know him better, and that he had to take that away. I miss him greatly and I'm mad that I'll never get to see his luck turn around. May he find peace in his rest.

**....**

The guy that pushed (nay, encouraged) me to do my first port. It's an unpleasant surprise to hear that he's gone. The first time I ever conversed with him through mail was because of some bug he found. I tested it on several different machines I had access to, and he mailed the results to the mailing list, with my name slap-bang on it. You can't get better credit than that. It's what made me interested in the KDE-on-FreeBSD in the first place.

While lurking (and sometimes delurking) on the mailinglists, you could notice he wasn't happy with the situation he was in. I could sympathize, since I lost my job too, and my life is pretty much a mess as well. What drove him to end it this way is beyond me, but I guess that's a choice he's made.

I will remember his wit and his insightful comments on technical things. Surely a great loss. I'll miss him too. I had mad respect for the man.

Rest in peace, Alan.

**....**

I had occasion to exchange email with Alan with questions (all mine) and he never failed to give good helpful answers. I'm no hacker, I just monitored the list to stay up on developments, but I learned a lot from his posts and those of others on the core team.

I knew he was having problems and was starting over in Denver: I wish it had worked out. He'll be missed.

**....**

I'm shocked. My first real contact with Alan was on IRC and he was full of life and fun to listen to him banter on about some topic that wasn't half as interesting as his thoughts on the topic. I remember when he told the world about being held up. Sadly, I remember the day when he was unjustly fired. I remember talking to him from the early evening and past sunrise about the best ways to write a compiler for Ruby, the speed of Forth, and how of all the ways to solve the problem of compiling Ruby into native ASM, Forth was likely the easiest solution available. I learned that crufty language because of him... something I'm sure I never would have done otherwise. I remember him passionately talking about real time trading floor software, ATM machines, and camera control systems and gleaning only the smallest fraction of the pearls of wisdom and experiences that he had squirreled away in his head. I remember him sending me his resume and my trying to peddle his skills when things were getting rough in New York. If I would have known how rough they were, I likely would have tried to find something a bit harder, I just assumed that he'd picked something up when he'd left NYC... I didn't talk with him much after he'd left the Big Apple. Someone as bright as he was, with a personality as strong as he had... Alan's deserved better and will be sorely missed. As a matter of fact, just this last week I was getting to the point in one of my personal projects that I was wondering where he was because I wanted to bounce some ideas off of him... ideas that'd come out of a conversation we'd had while talking one late evening... unfortunately that will never happen.

Alan, I'm sorry to hear you were that unhappy with yourself or the world: I hope you are at ease with yourself finally as I will miss you and our lengthy talks. I hear you had a temper or agitated other people... I never saw that side of you, though that could be that we were both bullheaded enough to never notice. I'm not sure I will ever forgive you for releasing your maintainership of the autofuck ports, especially in this manner. I wish you had a chance to get your ass back in the game and take care of them as I know of no one else who wanted to deal with that can of worms or did so with such conviction to make something so broken, work so seamlessly for the rest of us poor saps. Rest in peace Alan, the turbulence in your life as subsided. I hope the road you're on now is a happier, more peaceful one.

**....**

I never knew Alan personally and looking through my e-mail archive I only had one or two correspondences with him, but nonetheless his absence is a great loss to the community. It's my great hope that his next life will be filled with less pain than this one was.

It always amazes me how tragedies bring out the very best in people. Whether it be a neighbour who brings over food to a sick friend, or thousands of people spontaneously showing up at blood banks across the US after the terrorists attacks on September 11, 2001. So to all those who knew him best, please use this tragedy to bring out the best in yourselves.

**....**

I never knew Alan, and if I ever did talk to him I probably would of just joked around about stuff and never talked seriously. I have always wanted to be more serious and honest with people and get to know them better, but have only recently started to do so.

His story may confuse some, but it is a story that I know all too well. I have seen it countless times up front and I have overdosed a few times in my life. Only by the grace of God am I alive today. 

Life is difficult. If love and unity is the truth, than fear and separation is the lie. We can't stop someone from destroying their life, it isn't as though they wanted to do so. I am sure he was a wonderful person. 

My only regret is that I never got to know him, so I could tell him he was a wonderful person. But today at least he doesn't have to suffer. He isn't alone anymore. 

It is only through love of each other that we can find ourselves. Hopefully this is a wake-up to some. A reminded that it is ok to care about others, get to know them, and tell them how you feel.

**....**

I'm saddened! Alan and I had a fairly regular email relationship for the last year. I was wondering why he hadn't been answering my email.

I edit the Shell Corner column at UnixReview.com. Alan had 2 winning scripts - one in the December S.C. and one in my "Backups from UR.com". I had only a professional relationship with him. As a shell programmer, he wasn't in a class by himself, but it didn't take long to call the role. He was brilliant!

I'm going to miss writing with him.
